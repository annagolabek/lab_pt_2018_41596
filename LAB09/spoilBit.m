function[h]=spoilBit(idx,h)
    if (idx==0)
        idx=randi([1 7], 1, 1); %losowanie
        h(idx)=h(idx)
    else 
        h(idx)=~h(idx)
    end
end
