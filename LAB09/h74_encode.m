function [h]=h74_encode(d)
G=[1 1 0 1;
   1 0 1 1;
   1 0 0 0;
   0 1 1 1;
   0 1 0 0;
   0 0 1 0;
   0 0 0 1];
h=(G*d)';
h=mod(h,2);
end

%DOTYCZY KODU HAMMING74 (h74)
%1.nadawca dzieli strumien binarny na paczki po 4 bity
%2.pakuje 4 bity danych w strukture 7 bitowa opatrujac je bitami parzystosci
%3.dane transmitujemy do odbiorcy
%4.odbiorca przepisuje z transmisji bity danych we wlasna strukture 7 bitowa
%i ponownie wylicza bity parzystosci
%5. potem wykonuje operacje XOR pomiedzy wyliczonymi bitami parzystosci a
%bitami parzystosci ktore otrzymal w transmisji oraz zamienia wyznaczony
%ciag binarny (z operacji XOR) na liczbe dziesietna. Je�eli liczba
%dziesietna ==0 to znaczy, �e dane przesz�y poprawnie. W przeciwnym razie
%liczba wskazuje miejsce w strumieniu transmisyjnym, kt�re nalezy zanegowac
%(wymaga korekcji) (~ - tylda)

%2 pyt kontrolne: na czym polega modyfikacja kodu h74 na kod SECDED, na czym polega r�nica?
%SECDED - Single Error Correction, Double Error Detection