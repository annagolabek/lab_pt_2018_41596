function[A]=doDFT(a)
N=length(a);
A=zeros(N,1);
for k=1:N
    for n=1:N
        A(k)=A(k)+(a(n)*(cos(-2*pi*k*n/N)+1i*sin(-2*pi*k*n/N)));
    end
end
end