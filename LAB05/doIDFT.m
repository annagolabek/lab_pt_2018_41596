function[a]=doIDFT(A)
N=length(A);
a=zeros(N,1);
for n=1:N
    for k=1:N
        a(n)=a(n)+(A(k)*((cos(2*pi*k*n)/N)-1i*sin((2*pi*k*n)/N)));
    end
    a(n)=a(n)/N;
end
end