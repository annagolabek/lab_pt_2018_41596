function plotSignalModulations(t, CLK, TTL, MAN, NRZI, BAMI)
figure1 = figure('Name','Sygna� no?ny po modulacji sygna�em cyfrowym');
axes1 = axes('Parent',figure1);
hold(axes1,'on');
title('Sygna� no?ny po modulacji sygna�em cyfrowym');
zero_line = zeros(1,length(t));
plot(t,zero_line+11,'LineStyle','--','Color',[0.5 0.5 0.5]);
plot(t,CLK+11,'k');
plot(t,zero_line+9,'LineStyle','--','Color',[0.5 0.5 0.5]);
plot(t,TTL+9,'b');
plot(t,zero_line+7,'LineStyle','--','Color',[0.5 0.5 0.5]);
plot(t,MAN+7,'g');
plot(t,zero_line+4,'LineStyle','--','Color',[0.5 0.5 0.5]);
plot(t,NRZI+4,'k');
plot(t,zero_line+1,'LineStyle','--','Color',[0.5 0.5 0.5]);
plot(t,BAMI+1,'m');
set(axes1,'YTick',[1 4 7 9 11],'YTickLabel',...
    {'PSK','FSK','ASK','TTL','CLK'});
xlabel('t[s]');
xlim(axes1,[0 max(t)]);
ylim(axes1,[-0.5 12.5]);
box(axes1,'on');
grid on