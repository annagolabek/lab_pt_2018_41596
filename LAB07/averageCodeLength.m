function[L]=averageCodeLength(cb,spd)
cBook=flipud(cb);
L=0;
for i=1:length(cBook(:,2))
    L=L+length(cBook{i,2})*spd(i);
end
end