function [ encodeMsg ] = encodeHuff( codeBook, msg )
% ************************
% Encode Huffman
%  By Cezary Wernik
% ************************
    encodeMsg='';
    for n=1:length(msg)
        tmpIdx = strfind(codeBook, msg(n));
        Idx = find(not(cellfun('isempty', tmpIdx)))';
        encodeMsg = [encodeMsg, [codeBook{Idx,2}]];
    end
end