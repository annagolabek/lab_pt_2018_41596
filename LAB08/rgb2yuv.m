function [Y, U, V] = rgb2yuv( R, G, B )
Y = double(0.30*double(R)+0.59*double(G)+0.11*double(B));
U = 0.56*(double(B)-Y)+128;
V = 0.71*(double(R)-Y)+128;
end