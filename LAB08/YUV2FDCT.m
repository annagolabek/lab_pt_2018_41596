function [ fY, fU, fV ] = YUV2FDCT(  mY, mU, mV  )
fY = FDCT(mY);
fU = FDCT(mU);
fV = FDCT(mV);
end