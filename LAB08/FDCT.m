function [F]=FDCT(f)
for u=1:8
    for v=1:8
        tmp=0;
        for x=1:8
            for y=1:8
                tmp=(f(x,y)*cos(2*(-x)+1))*((u-1)*pi/16)*cos((2*(-y)+1)*(v-1)*pi/16)+tmp;
            end
        end
        F(u,v)=tmp*(1/4)*C(u-1)*C(v-1);
    end
end
end