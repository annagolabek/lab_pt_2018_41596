function [ uY, uU, uV ] = sign2unsign( zY, zU, zV )
   idx = find(zY<0);
   uY=abs(zY).*2;
   uY(idx)= uY(idx)+1;
   
   idx = find(zU<0);
   uU=abs(zU).*2;
   uU(idx)= uU(idx)+1;
   
   idx = find(zV<0);
   uV=abs(zV).*2;
   uV(idx)= uV(idx)+1;
end