function [ttl] = ttlGenerator(CLK,data)
N=length(CLK);
y=1;
x=1;
for n=1:N
    if (n+1<N)
        if (CLK(n)<CLK(n+1))
            x=x+1;
            if (data(x)==1)
                y=1;
            else
                y=0;
            end
        end
    end
    ttl(n)=y;
end
end
