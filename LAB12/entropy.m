function [H] = entropy( spd )
H=-sum(spd.*log2(spd));
end