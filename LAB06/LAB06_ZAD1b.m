[lr,Fs]=audioread('everything-is-fine.wav');

l=lr(:,1);
figure;
subplot(5,1,1);
plot(l),title("Orginal"),ylabel("A"),xlabel("t");

% Wczytanie pliku
[lr,Fs]=audioread('everything-is-fine.wav');

%Wyb�r tylko jednego kana�u
l=lr(:,1);

% Obliczenie Fs-punktowej transformaty
L=fft(l,Fs);

% Odtworzenie sygna�u l, bez zerowania sk��dowych
sound(l,Fs)

% Odczekanie
pause(0.2);

% Roboczy/pomocniczy wsp�czynnik odci�cia <0,1>
q=0.9;

% Obracanie widma
L=fftshift(L);
subplot(5,1,2),plot(abs(L)),title("Odwr�cone widmo"),ylabel("M"),xlabel("f");

% Obliczenie progu odci�cia i wyzerowanie kranc�w widma
r=floor((Fs/2)*q);
L(1:r)=0;
L(Fs-r:Fs)=0;
subplot(5,1,3),plot(abs(L)),title("Wyzerowane kra�ce"),ylabel("M"),xlabel("f");

% Obracanie widma do pierwotnej kolejno�ci
L=fftshift(L);
subplot(5,1,4),plot(abs(L)),title("Ponowne odwr�cenie widma"),ylabel("M"),xlabel("f");

% Odwrotna transformata
l=real(ifft(L));
subplot(5,1,5),plot(l),title("Tranformacja odwrotna"),ylabel("A"),xlabel("t");

% Odtworzenie sygna�u l z wyzerowan� cz�ci� widma
sound(l,Fs);