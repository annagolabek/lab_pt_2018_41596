clear all
close all
clc

%Wczytanie pliku
[lr,Fs]=audioread('everything-is-fine.wav');

%Wyb�r tylko jednego kana�u
l=lr(:,1);
figure(1),plot(l),title("Signal"),ylabel("A"),xlabel("t");

samples(10000,16000);
[lr2,Fs]=audioread('everything-is-fine.wav');
l2=lr2(:,1);
figure(2),plot(l2),title("Fragment"),ylabel("A"),xlabel("t");

X=doDFT(l2);
x=abs(X);
y=0:length(X)-1;
figure(3),plot(x,y),title("DFT"),ylabel("M"),xlabel("f");

dB=mag2db(x);
figure(4),plot(x,dB),title("Decybele"),ylabel("M(DB)"),xlabel("f");

