function[X]=doDFT(x)
N=length(x);
X=zeros(N,1);
for a=1:N
    for b=1:N
        X(a)=X(a)+(x(b)*(cos(-2*pi*a*b/N)+1i*sin(-2*pi*a*b/N)));
    end
end
end